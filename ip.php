<?php
$browser = get_browser();
$real_browser = !in_array(strtolower($browser->browser), ["default browser", "curl", "wget"]);

if (array_key_exists("HTTP_CF_CONNECTING_IP", $_SERVER)) {
  $remote_ip = "HTTP_CF_CONNECTING_IP";
} else {
  $remote_ip = "REMOTE_ADDR";
}
$ip_address = $_SERVER[$remote_ip];

if ($real_browser) {
  include_once "common-header.php";
?>
<main role="main">
  <div class="container">
    <pre>ip address: <?php print($ip_address); ?></pre>
    <pre><?php print_r($browser); ?></pre>
  </div>
</main>

<?php

  include_once "common/footer.php";
} else {
    print($ip_address);
}
?>
