<?php
use PHPUnit\Framework\TestCase;

require_once("functions.php");

class FunctionsTest extends TestCase
{
    public function testMetadata()
    {
        $output = "test, foo";

        $this->assertSame($output, generate_meta('test', 'foo'));
    }

    public function testActiveClass()
    {
      $_SERVER["HTTP_HOST"] = "testcase.com";

      $activeOutput = 'class="nav-item active"';
      $output = 'class="nav-item"';

      $_SERVER["SCRIPT_NAME"] = "blah.php";
      $this->assertSame($output, active_class("/"));
      $this->assertSame($activeOutput, active_class("blah"));
      $this->assertSame($output, active_class("notblah"));

      $_SERVER["SCRIPT_NAME"] = "index.php";
      $this->assertSame($activeOutput, active_class("/"));
      $this->assertSame($output, active_class("any"));
    }
}
?>
