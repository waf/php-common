<?php
function errorMessage($code)
{
    $error_arr = [
        401 => [
            "short_message"  => "Unauthorized Access",
            "full_message"  => "",
        ],
        403 => [
            "short_message"  => "Forbidden Access",
            "full_message"  => "",
        ],
        404 => [
            "short_message"  => "Page Not Found",
            "full_message"  => "This page is not found.",
        ],
        500 => [
            "short_message"  => "Internal Server Error",
            "full_message"  => "",
        ],
        502 => [
            "short_message"  => "Bad Gateway",
            "full_message"  => "",
        ],
        503 => [
            "short_message"  => "Service Unavailable",
            "full_message"  => "The backend server is not available. Please try again later.",
        ],
        504 => [
            "short_message"  => "Gateway Timeout",
            "full_message"  => "The backend server failed to respond in time",
        ],
    ];

    if ($error_arr[$code]) {
        return [error_arr[$code]["short_message"], error_arr[$code]["full_message"]];
    } else {
        return ["ERROR", "SOME ERROR!"];
    }
}

$redirect_status = -42; #$_SERVER["REDIRECT_STATUS"];

[$short_message, $full_message] = errorMessage($redirect_status);

$title = $short_message;

$common_header_file = dirname(__FILE__, 2) . "/common-header.php";
if (file_exists($common_header_file)) {
  include_once $common_header_file;
} else {
  include_once "header.php";
}
?>

  <main role="main">
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Something Error Happened</h1>
      </div>

      <p class="lead">Error Code <?php echo $redirect_status; ?> - <?php echo $short_message; ?></p>
      <p><?php echo $full_message; ?></p>
    </div>
  </main>

<?php
include_once "footer.php";
?>
