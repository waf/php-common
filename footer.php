<?php
$custom_footer_file = dirname(__FILE__, 2) . "/custom-footer.php";

if (file_exists($custom_footer_file)) {
  include_once $custom_footer_file;
} else {
?>
    <footer class="footer">
      <div class="container">
        <p class="text-muted credit"><a href="/privacy-policy" target="_blank">Privacy Policy</a> - <a href="/terms-of-service" target="_blank">Terms of Service</a></p>
<?php
    global $copyright_year;
    global $footers;

    $copyright_year = $copyright_year ?: "2007";

    if ($footers) {
        if (gettype($footers) == "string") {
            $footers = [$footers];
        }

        foreach ($footers as $footer) {
?>
        <p class="text-muted credit"><?php echo $footer; ?></p>
<?php
        }
    }
?>
        <p class="text-muted credit">Copyright &copy; <?php echo $copyright_year; ?>-present <?php echo generateSiteName(); ?>. All Rights Reserved.
</p>
      </div>
    </footer>

    <!-- ============= Bootstrap core JavaScript =========== -->
    <script src="https://code.jquery.com/jquery-3.6.3.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-28790107-1', 'fawong.com');
    ga('send', 'pageview');
    </script>
  </body>
</html>
<?php
}
?>
