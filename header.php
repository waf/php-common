<?php
include_once "functions.php";

global $links;
global $dropdown_links;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo get_site(); ?> Homepage, <?php echo generate_meta(); ?>">
    <meta name="author" content="<?php echo generate_meta(); ?>" />
    <link rel="shortcut icon" href="<?php echo get_favicon(); ?>">

    <title><?php echo generate_site_name(); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">


    <!-- Custom styles for this template -->
    <link href="/common/assets/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../common/assets/js/html5shiv.js"></script>
    <script src="../../common/assets/js/respond.min.js"></script>
    <![endif]-->

    <script>
    var _prum = [['id', '5205f924abe53d514a000000'],
                 ['mark', 'firstbyte', (new Date()).getTime()]];
    (function() {
        var s = document.getElementsByTagName('script')[0]
          , p = document.createElement('script');
        p.async = 'async';
        p.src = '//rum-static.pingdom.net/prum.min.js';
        s.parentNode.insertBefore(p, s);
    })();
    </script>
    <?php
    if (isset($extra_headers)) {
      echo $extra_headers;
    }
    ?>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
        <div class="container">
          <a class="navbar-brand" href="/"><?php echo get_site_name(); ?></a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mr-auto">
<?php
    if ($links) {
        foreach ($links as $link => $text) {
?>
              <li <?php echo active_class($link); ?>><a class="nav-link" href="<?php echo $link; ?>"><?php echo $text; ?></a></li>
<?php
        }
    }
    if ($dropdown_links) {
?>
              <li class="nav-item dropdown">
                <a href="#" class="dropdown-toggle nav-link" id="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Other Links <b class="caret"></b></a>
                <div class="dropdown-menu" aria-labelledby="dropdown">
<?php
      foreach ($dropdown_links as $link => $text) {
?>
                  <a class="dropdown-item" href="<?php echo $link; ?>" target="_blank"><?php echo $text; ?></a>
<?php
        }
?>
                </div>
              </li>
<?php
    }
?>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>
    </header>
